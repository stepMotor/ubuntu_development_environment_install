#!/bin/bash

SUCCESS=0 
function checkInst()
{
    #apt-get -y --force-yes install 
	for item in $1 
	do
		echo "install =========================== " ${item} " =============================="
		#printf "hello %.5s\n" message
		apt-get -y --force-yes install ${item}
		if [ "$?" -ne $SUCCESS ]
		then
			echo "ERROR Install " ${item}
			read line1
		fi
	done
}

tools="ftp telnet nmap tftp ntpdate screen lsof manpages manpages-dev manpages-posix manpages-posix-dev strace ltrace chkconfig"
xdesktop="xserver-xorg-core xfce4 xfce4-terminal xfburn thunar-archive-plugin gdm "
editor="galculator vim vim-gtk medit rdesktop xvnc4viewer filezilla claws-mail claws-mail-i18n claws-mail-tnef-parser claws-mail-html2-viewer"
xtools="ristretto fbreader p7zip-full arj zip mscompress file-roller stardict-gtk iptux"
chineseinput="fcitx"
mediatools="alsa-base alsa-utils mplayer mencoder smplayer ffmpeg"
develops="astyle ctags cxref ccache gettext poedit gdb cppcheck build-essential graphviz intltool valgrind"
fssupport="fusesmb smbclient smbfs ntfs-3g sshfs openssh-client openssh-server"
nettools="ethtool wireless-tools wicd mtools dosfstools pppoe"
sniffer="wireshark"
desk3d="compiz compiz-fusion-plugins-extra compiz-fusion-plugins-main compiz-fusion-bcop compizconfig-settings-manager cairo-dock-compiz-icon-plugin"
others="rpm qbittorrent"
curlpkg="aria2 axel curl mpg321 easymp3gain-gtk jp2a nmon arping conky libnotify-bin inotify-tools dia dnsutils"


checkInst "${tools}"
checkInst "${xdesktop}"
checkInst "${editor}"
checkInst "${xtools}"
checkInst "${chineseinput}"
checkInst "${mediatools}"
checkInst "${develops}"
checkInst "${fssupport}"
checkInst "${sniffer}"
checkInst "${nettools}"
checkInst "${others}"
checkInst "${desk3d}"
checkInst "${curlpkg}"

apt-get install firmware-realtek firmware-linux-nonfree firmware-linux-free libqt4-opengl

apt-get -y --force-yes remove vim-tiny nano tasksel tasksel-data jfbterm zhcon --purge
apt-get -y --force-yes remove aumix ttf-arphic-ukai ttf-arphic-uming iceweasel --purge
apt-get autoremove --purge -y --force-yes
apt-get upgrade -y --force-yes